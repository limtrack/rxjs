// Libraries
const path = require("path");
const fs = require("fs");
const HtmlWebpackPlugin = require("html-webpack-plugin");
// Set paths
const appDirectory = fs.realpathSync(process.cwd());
const resolvePath = (relativePath) => path.resolve(appDirectory, relativePath);
// Host
const host = process.env.HOST || "localhost";
const mode = process.env.MODE || "development";

module.exports = {
  mode,
  entry: {
    app: resolvePath("src/index.js"),
  },
  output: {
    path: resolvePath("dist"),
    filename: "app.[hash].js",
  },
  devtool: "source-map",
  devServer: {
    contentBase: resolvePath("public"),
    hot: true,
    host,
    port: 3000,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: [resolvePath("src")],
        exclude: [resolvePath("node_modules")],
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
          },
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: resolvePath("public/index.html"),
      filename: "index.html",
    }),
  ],
};
