import React from "react";
// Router
import { BrowserRouter } from "react-router-dom";
// Components
import { Layout } from "antd";
import AppHeader from "./components/AppHeader";
import AppRoutes from "./components/AppRoutes";
// Context
import { ContextProvider } from "./context";

const { Header, Content } = Layout;

const App = () => {
  return (
    <BrowserRouter>
      <ContextProvider>
        <Layout className="app">
          <Header className="app-header">
            <AppHeader />
          </Header>
          <Content className="app-content">
            <AppRoutes />
          </Content>
        </Layout>
      </ContextProvider>
    </BrowserRouter>
  );
};

export default App;
