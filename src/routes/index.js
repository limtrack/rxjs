import Home from "../pages/Home";
import Code from "../pages/Code";
import About from "../pages/About";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    exact: true,
  },
  {
    path: "/code",
    name: "Code",
    component: Code,
    exact: true,
  },
  {
    path: "/about",
    name: "About",
    component: About,
    exact: true,
  },  
];

export default routes;
