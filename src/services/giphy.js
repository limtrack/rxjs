import { ajax } from 'rxjs/ajax';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

const constants = require('../constants');

/**
 * Search in Giphy API a amount of gifs
 * 
 * Possible options -> https://developers.giphy.com/docs/api/endpoint
 * 
 * @param {String} q - query to search
 * @param {Object} options - options to set in the request
 */
export const searchGifs = (
  q = '',
  options = { limit: 25, offset: 0, lang: 'es' }
) => {
  const apiUrl = `${constants.giphy.url}gifs/search`;
  const paramsUrl = Object.entries(options)
    .reduce((sumOptions, option) => {
      return sumOptions = sumOptions.concat(`&${option[0]}=${option[1]}`);
    }, `?api_key=${constants.giphy.key}&q=${q}`);
  
  return ajax(`${apiUrl}${paramsUrl}`)
    .pipe(
      map((res) => {
        return res.response.data.map(item => {
          return {
            id: item.id,
            title: item.title,
            img: item.images.downsized_medium.url,
          };
        });
      }),
      catchError(error => {
        return of(error);
      })
    );
}