import React from "react";
// Router
import { Route, Switch, Redirect } from "react-router-dom";
import routes from "../../routes";
// Components
import NotFound from "../../pages/NotFound";
// Styles
import "./AppRoutes.css";

const AppRoutes = () => (
  <div className="routes">
    <Switch>
      {routes.map(({ exact, path, component }, i) => (
        <Route exact={exact} path={path} component={component} key={i} />
      ))}
      <Route path="/404" component={NotFound} />
      <Redirect to="/404" />
    </Switch>
  </div>
);

export default AppRoutes;
