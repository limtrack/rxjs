import React from "react";
// Components
import { Row, Col } from "antd";
import AppMenu from "../AppMenu";
// CSS styles
import "./AppHeader.css";

const AppHeader = () => (
  <Row>
    <Col className="app-header-title" span={12}>GIPHY with RxJS</Col>
    <Col className="app-header-menu" span={12}>
      <AppMenu />
    </Col>
  </Row>
);

export default React.memo(AppHeader);
