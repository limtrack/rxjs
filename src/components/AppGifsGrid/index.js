import React, { useMemo, useEffect, useState, useContext } from "react";
// Components
import AppGif from "../AppGif";
// Context
import { Context } from "../../context";
// Services
import { searchGifs } from "../../services/giphy.js";
// Styles
import "./AppGifsGrid.css";

const AppGifsGrid = ({ ...props }) => {
  // State
  const {
    state: { query },
  } = useContext(Context);
  const [gifs, setGifs] = useState([]);
  // Observables
  const fetchGifsMemo$ = useMemo(
    () => searchGifs(query, { limit: 25, offset: 0 }),
    [query]
  );

  // Subscribe & Unsubscribe
  useEffect(() => {
    let subscriptionGifs = null;

    query
      ? subscriptionGifs = fetchGifsMemo$.subscribe((res) => {
          setGifs(res);
      })
      : setGifs([]);

    return () => subscriptionGifs && subscriptionGifs.unsubscribe();
  }, [query, fetchGifsMemo$]);

  return (
    <div className="grid" {...props}>
      {!query ? (
        <span>You must write some term to search</span>
      ) : gifs.length ? (
        gifs.map((gif) => {
          return <AppGif key={gif.id} img={gif.img} title={gif.title} />
        })
      ) : (
        <span>There wasn't result to the query</span>
      )}
    </div>
  );
};

export default AppGifsGrid;
