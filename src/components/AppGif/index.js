import React from "react";
// Styles
import "./AppGif.css";

const AppGif = ({ img, title }) => {
  return (
    <div className="gif">
      <img
        src={img}
        alt={title}
        title={title}
        loading="lazy"
      />
    </div>
  );
};

export default AppGif;
