import React, { useContext, useRef, useEffect } from "react";
// RxJS v6+
import { fromEvent, timer } from "rxjs";
import { debounce, filter } from "rxjs/operators";
// Context
import { Context } from "../../context";
// Components
import { Input } from "antd";
// CSS styles
import "./AppSearch.css";

const { Search } = Input;

const AppSearch = () => {
  const {
    state: { query },
    dispatch,
  } = useContext(Context);
  const inputSearch = useRef(null);
  // Set query to search
  const setQueryToSearch = (value) => {
    dispatch({
      type: "SET_QUERY",
      value,
    });
  };

  useEffect(() => {
    // Input search native
    const inputSearchNative = inputSearch.current.input.input;
    // Observer event with RxJS
    const observerInputSearch$ = fromEvent(inputSearchNative, "keyup");
    // Filters to use with observer
    const filterInputSeach = observerInputSearch$.pipe(
      debounce(() => timer(500)),
      filter((e) => e.target.value.length > 2)
    );
    // Subscribe to observer
    const subscribeInputSeach = filterInputSeach.subscribe((e) => {
      setQueryToSearch(e.target.value);
    });

    // Unsubscribe
    return () => subscribeInputSeach.unsubscribe();
  }, []);

  return (
    <div className="app-search">
      <Search
        ref={inputSearch}
        placeholder="Search by..."
        defaultValue={query}
      />
    </div>
  );
};

export default AppSearch;
