import React, { useState } from "react";
// Routes
import { Link } from "react-router-dom";
// Components
import { Menu } from "antd";
import {
  FileImageOutlined,
  RobotOutlined,
  GithubOutlined,
} from "@ant-design/icons";
// CSS styles
import "./AppMenu.css";

const AppMenu = () => {
  const [selectedItem, setSelectedItem] = useState("home");
  const handleClick = (e) => {
    setSelectedItem(e.key);
  };

  return (
    <Menu
      onClick={handleClick}
      selectedKeys={[selectedItem]}
      mode="horizontal"
      className="app-menu"
    >
      <Menu.Item key="home">
        <FileImageOutlined />
        <Link to="/">Home</Link>
      </Menu.Item>
      <Menu.Item key="code">
        <GithubOutlined />
        <Link to="/code">Code</Link>
      </Menu.Item>
      <Menu.Item key="author">
        <RobotOutlined />
        <Link to="/about">About me</Link>
      </Menu.Item>
    </Menu>
  );
};

export default AppMenu;
