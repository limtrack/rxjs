const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_QUERY': {
      return {
        ...state,
        query: action.value,
      };
    }
    default:
      return state;
  }
};

export default reducer;
