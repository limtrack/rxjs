import React, { useReducer } from 'react';
import reducer from './reducer';

const initState = require('./state');

export const Context = React.createContext({
  state: initState,
  dispatch: null,
});

export const ContextProvider = (props = {}) => {
  const [state, dispatch] = useReducer(reducer, initState);
  return (
    <Context.Provider value={{ state, dispatch }}>
      {props.children}
    </Context.Provider>
  );
};
