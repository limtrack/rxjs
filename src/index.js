import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";
// CSS styles
import "../node_modules/antd/dist/antd.css";
import "./styles.css";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
