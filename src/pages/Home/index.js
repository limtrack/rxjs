import React, { Suspense, lazy } from "react";
// Components
import { Typography } from "antd";
// Styles
import "./Home.css";
// Components
const AppSearch = lazy(() => import("../../components/AppSearch"));
const AppGifsGrid = lazy(() => import("../../components/AppGifsGrid"));
const { Title } = Typography;

const Home = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <div className="home">
        <AppSearch />
        <div className="home-content">
          <Title level={3} data-testid="page-title">Home</Title>
          <AppGifsGrid />
        </div>
      </div>
    </Suspense>
  );
};

export default Home;
