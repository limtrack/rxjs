import React from "react";
// Components
import { Typography } from "antd";
// Styles
import "./Code.css";

const { Title, Paragraph } = Typography;

const Code = () => {
  return (
    <div className="code">
      <Title level={3}>Code</Title>
      <Paragraph>
        It is a very simple test to check how is work RxJS with React. You can
        find this code in my{" "}
        <a href="https://bitbucket.org/limtrack/rxjs/src/master/">
          bitbucket repository
        </a>
      </Paragraph>
      <Paragraph>
        I used for the test: React, Antd (framework CSS), Giphy API and RxJS.
      </Paragraph>
    </div>
  );
};

export default Code;
