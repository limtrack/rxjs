import React from "react";
// Components
import { Typography } from 'antd';
// Styles
import "./NotFound.css";

const { Title } = Typography;

const NotFound = () => {
  return (
    <div className="not-found">
      <Title level={3}>Not Found - 404</Title>
    </div>
  );
}

export default NotFound;
