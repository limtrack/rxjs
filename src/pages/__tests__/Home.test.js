import React from 'react';
import {render} from '@testing-library/react';
import Home from '../Home';

describe('Testing Home page', () => {
  it('The renderind is correct', () => {
    const { queryByTestId } = render(
      <Home />,
    );
    expect(queryByTestId('page-title')).toBeInTheDocument();
  });
});
