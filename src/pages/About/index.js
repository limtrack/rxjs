import React from "react";
// Components
import { Typography } from 'antd';
// Styles
import "./About.css";

const { Title, Paragraph } = Typography;

const About = () => {
  return (
    <div className="about">
      <Title level={3}>About Me</Title>
      <Paragraph>
        My name is Antonio Irlandés García and I am a frontend developer,
        you can find more about me in my <a href="http://programador-frontend.com">online CV</a>
      </Paragraph>
    </div>
  );
}

export default About;
